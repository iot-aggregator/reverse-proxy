FROM nginx:1.17.10-alpine

COPY ./config/nginx.conf /etc/nginx/nginx.conf

COPY ./custom-entrypoint.sh /custom-entrypoint.sh

ENTRYPOINT ["sh", "/custom-entrypoint.sh"]