#!/bin/sh

sed -i "s|CONSUL_ADDRESS|${CONSUL_ADDRESS}|g" /etc/nginx/nginx.conf
sed -i "s|CONSUL_PORT|${CONSUL_PORT}|g" /etc/nginx/nginx.conf
sed -i "s|MINIO_ADDRESS|${MINIO_ADDRESS}|g" /etc/nginx/nginx.conf
sed -i "s|MINIO_PORT|${MINIO_PORT}|g" /etc/nginx/nginx.conf
sed -i "s|INTEGRATION_SERVICE_ADDRESS|${INTEGRATION_SERVICE_ADDRESS}|g" /etc/nginx/nginx.conf
sed -i "s|INTEGRATION_SERVICE_PORT|${INTEGRATION_SERVICE_PORT}|g" /etc/nginx/nginx.conf
sed -i "s|PROMETHEUS_ADDRESS|${PROMETHEUS_ADDRESS}|g" /etc/nginx/nginx.conf
sed -i "s|PROMETHEUS_PORT|${PROMETHEUS_PORT}|g" /etc/nginx/nginx.conf
sed -i "s|GRAFANA_ADDRESS|${GRAFANA_ADDRESS}|g" /etc/nginx/nginx.conf
sed -i "s|GRAFANA_PORT|${GRAFANA_PORT}|g" /etc/nginx/nginx.conf
sed -i "s|WEB_ADDRESS|${WEB_ADDRESS}|g" /etc/nginx/nginx.conf
sed -i "s|WEB_PORT|${WEB_PORT}|g" /etc/nginx/nginx.conf

nginx -g "daemon off;"